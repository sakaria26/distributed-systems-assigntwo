# Collections
- Customer
- Order
- Product
- Store

## Customer
- id
- name
- email
- phone
- address
- password
- orders[]

## Order
- id
- customer_id
- products[]
- total_quantity
- total_price

## Product
- id
- name
- price
- quantity

## Store
- id
- name
- address
- phone_number
- products[]