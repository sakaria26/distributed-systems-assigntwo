import ballerinax/kafka;
public enum Order_Status {
    PLACED,
    APPROVED,
    DELIVERED
}

public type Product record {|
    string id;
    string name;
    decimal price;
    int quantity;
|};

public type Order record {|
    string id;
    string customerId;
    string[] products;
    int totalQuantity;
    decimal totalPrice;
    Order_Status status;

|};

public type Customer record {|
    string id;
    string name;
    string email;
    string phone;
    string address;
    string password;
    Order[] orders;
|};
public type CustomerQuery record {|
    string id;
    string password;
|};

public type Store record {|
    string id;
    string name;
    string location;
    string phone;
    Product[] products;
|};

public type Configuration record {|
    string host;
    int port;
    string database;
    string collection;
|};

public kafka:ProducerConfiguration producerConfigs = {
    clientId: "order-producer",
    acks: "all",
    retryCount: 3
};

public kafka:ConsumerConfiguration consumerConfigs = {
    clientId: "order-consumer",
    groupId: "order-group",
    offsetReset: "earliest",
    pollingInterval: 1,
    topics: ["order_req", "order_res"],
    autoCommit: false
};