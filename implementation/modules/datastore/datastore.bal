import implementation.model as Model;
import ballerinax/mongodb;

public class Store {
    private mongodb:Client mongoClient;
    private string collection;

    public function init(Model:Configuration configuration) {
        mongodb:ConnectionConfig mongoConfig = {
            host: configuration.host,
            port: configuration.port
        };
        self.mongoClient = checkpanic new (mongoConfig, configuration.database);
        self.collection = configuration.collection;
    }

    # Description
    # Logs the customer into the system
    # + id - id of the customer
    # + password - password of the customer
    # + return - Returns a boolean if the customer exists or not, or returns an error
    public isolated function login(string id, string password) returns boolean|error {
        Model:CustomerQuery customer = {
            id: id,
            password: password
        
        };

        stream<Model:Customer, error?> result = checkpanic self.mongoClient->find(self.collection, (), customer, 'limit = 1);

        record {|Model:Customer value;|}|error? res = result.next();

        while (res is record {|Model:Customer value;|}) {
            Model:Customer cust = res.value;
            if (id == cust.id && password == cust.password) {
                return true;
            }
        }
        return false;
    }

    public isolated function updateProduct(string productId, int newTotal){
        map<json> product = {
            id: productId
        };

        stream<Model:Product, error?> result = checkpanic self.mongoClient->find(self.collection, (), product, 'limit = 1);

        record {|Model:Product value;|}|error? res = result.next();

        while (res is record {|Model:Product value;|}) {
            Model:Product prod = res.value;
            if (productId == prod.id) {
                prod.quantity = newTotal;
                map<json> update = {
                    id: productId,
                    quantity: newTotal
                };
                _ = checkpanic self.mongoClient->update(update, self.collection, (), product);
            }
        }
    }

    # Description
    # saves the order to the database
    # + customerOrder - order to be saved
    # + return - Returns an error if the order cannot be saved
    public isolated function saveOrder(Model:Order customerOrder) returns error? {
        return checkpanic self.mongoClient->insert(customerOrder, "Order");
    }

    # Description
    # Retrieves all products from the database
    # + return - Returns a stream of products or an error
    public isolated function allProducts() returns Model:Product[] {
        stream<Model:Product, error?> products = checkpanic self.mongoClient->find("Product", (), ());
        Model:Product[] product = [];

        record {|Model:Product value;|}|error? res = products.next();

        while (res is record {|Model:Product value;|}) {
            product.push(res.value);
            res = products.next();
        }

        return product;
    }
    public isolated function numberOfAllProducts() returns int {
        stream<Model:Product, error?> products = checkpanic self.mongoClient->find("Product", (), ());
        int currentTotal = 0;

        record {|Model:Product value;|}|error? res = products.next();

        while (res is record {|Model:Product value;|}) {
            currentTotal += res.value.quantity;
            res = products.next();
        }

        return currentTotal;
    }

    

    public isolated function retreiveStores() returns Model:Store[] {
        stream<Model:Store, error?> stores = checkpanic self.mongoClient->find("Store", (), ());
        Model:Store[] store = [];

        record {|Model:Store value;|}|error? res = stores.next();

        while (res is record {|Model:Store value;|}) {
            store.push(res.value);
            res = stores.next();
        }

        return store;
    } 

    public isolated function findSingleProduct(string productID) returns Model:Product|error{
        map<json> productQueryString = {"id": productID};
        stream<Model:Product, error?> products = checkpanic self.mongoClient->find("Product", (), productQueryString, 'limit = 1);

        record {|Model:Product value;|}|error? res = products.next();

        while (res is record {|Model:Product value;|}) {
            Model:Product prod = res.value;
            if (productID == prod.id) {
                return prod;
            }
        }
        return error("Product not found");
    }


}
