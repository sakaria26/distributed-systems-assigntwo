import ballerina/io;
import implementation.datastore as Datastore;
import implementation.model as Model;
import ballerinax/kafka;
import ballerina/log;
import implementation.util as Util;

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL, Model:producerConfigs);
kafka:Consumer kafkaConsumer = check new (kafka:DEFAULT_URL, Model:consumerConfigs);
listener kafka:Listener Listener = new (kafka:DEFAULT_URL, Model:consumerConfigs);

service /kafka on Listener {
    private Datastore:Store store;
    private Model:Product[] productList;
    private Model:Configuration config = {
        host: "localhost",
        port: 27017,
        database: "store",
        collection: "Customer"
    };

    function init() {
        self.store = new (self.config);
        self.productList = self.store.allProducts();
    }

    function processOrder(kafka:ConsumerRecord consumerRecord, Datastore:Store store) returns error|error {
        byte[] payload = consumerRecord.value;

        string id = "SN123456";
        string password = "password";
        boolean keepOrdering = true;
        string[] products = [];
        decimal totalPrice = 0.0;
        int totalQuantity = 0;
        if (id == "" || password == "") {
            io:println("Please enter your Account ID Number and Password");
            return error("Please enter your Account ID Number and Password");
        } else if (check self.store.login(id, password)) {
            io:println("Login Successful");

            io:println("Welcome to the Store");
            io:println("Select the item you wish to purchase");

            foreach var item in self.productList {
                io:println(string `Product Name : ${item.name} | Price : ${item.price} -> ${item.id}`);
            }

            while (keepOrdering) {
                string productIdChoice = "1";

                Model:Product|error productChoice = self.store.findSingleProduct(productIdChoice);

                if (productChoice is error) {
                    log:printInfo("Invalid Choice");
                } else {
                    int|io:Error quantity = 10;

                    if quantity is int {
                        if quantity > productChoice.quantity {
                            io:println("The quantity you have entered is not available! Please Pick an option to continue");
                            io:println("1) Adjust Quantity");
                            io:println("2) Remove Item");
                            io:println("3) Cancel Order");

                            string option = io:readln("Enter your option : ");
                            if option == "1" {
                                quantity = check int:fromString(io:readln("Enter the quantity : "));
                                if quantity is int {
                                    log:printInfo("Invalid Quantity Try Again");
                                } else {
                                    products.push(string `${productIdChoice} + " : " + ${quantity.toString()}`);
                                }
                            } else if option == "2" && quantity is int {
                                string removeOrder = string `${productIdChoice} + " : " + ${quantity.toString()}`;
                                products = products.filter(i => i != removeOrder);
                            } else if option == "3" {
                                return error("Order Cancelled");
                            } else {
                                log:printInfo("Invalid Option");
                            }
                        }

                        if quantity is int {
                            int newTotal = productChoice.quantity - quantity;
                            store.updateProduct(productIdChoice, newTotal);
                        }
                    } else {
                        log:printInfo("Invalid Quantity");
                    }

                    log:printInfo("Do you with to continue? (y/n)");
                    keepOrdering = false;
                    

                    if quantity is int {
                        totalQuantity = totalQuantity + quantity;
                        totalPrice = totalPrice + (productChoice.price * quantity);
                    }
                }
            }

            log:printInfo(string `Your total is N$ ${totalPrice} and you have ordered ${totalQuantity} items`);
            log:printInfo("Do you want to confirm your order? (y/n) : ");

            
                Model:Order customerOrder = {
                id: check Util:generateOrderID(id, "X5", store.numberOfAllProducts()),
                customerId: id,
                products: products,
                totalQuantity: totalQuantity,
                totalPrice: totalPrice,
                status: Model:PLACED
            };

                _ = check store.saveOrder(customerOrder);
                string customers_order = string `Order ID: ${id} | Products: ${products.toString()} | Total: N$ ${totalPrice} | Total Items: ${totalQuantity}`;
                kafka:ProducerRecord producerRecord = { topic: "order_response", value: customers_order.toBytes() };
                var sendResult = kafkaProducer->send(producerRecord);
                if (sendResult is error) {
                    log:printInfo("Error sending the message");
                }

            

        } else {
            io:println("Login Failed");
            return error("Login Failed");
        }

        return error("Order Processed");

    }
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] consumerRecord) returns error? {
        foreach kafka:ConsumerRecord item in consumerRecord {
            error orderResults = self.processOrder(item, self.store);
            log:printInfo(orderResults.message());
        }
    }
}
